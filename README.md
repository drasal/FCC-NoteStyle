# FCC-hh Note Style

This project represents a collection of **paper/note templates** (see [FCChhTemplate](FCChhTemplate) 
directory) & **ROOT plot styles** (see [FCChhStyle](FCChhStyle) directory) to be 
used within the FCC-hh Collaboration for a consistent documentation of their 
on-going work on the FCC-hh project. The style is particularly intended for the 
CDR (Conceptual Design Report) supporting notes. These notes shall be normally 
reviewed by the Collaboration and then published on the CERN Document Server (CDS). 
For more information, please, follow: 

http://cern.ch/fcc/collaboration & [CDS Submit](https://cds.cern.ch/submit?doctype=FCC) 
procedure

The overall template/style has been kindly derived from an official **CLICdp 
Collaboration templates/styles** https://gitlab.cern.ch/CLICdp/Publications/Templates/Style 
(courtesy of [CLICdp Collaboration](https://clicdp.web.cern.ch/)).

For more information on how-to use the template/styles:

* For notes/papers follow a detailed instruction PDF file [FCChh_template.pdf](FCChhTemplate/FCChh_template.pdf)
* For ROOT plots style follow instructions in [README](FCChhStyle/README) 
& [Instr.txt](FCChhStyle/Instr.txt) files 